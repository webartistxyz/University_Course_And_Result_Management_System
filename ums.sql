-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2017 at 07:09 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ums`
--

-- --------------------------------------------------------

--
-- Table structure for table `allocate_rooms`
--

CREATE TABLE `allocate_rooms` (
  `id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `value` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `allocate_rooms`
--

INSERT INTO `allocate_rooms` (`id`, `dept_id`, `course_id`, `room_id`, `day_id`, `start`, `end`, `value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(87, 14, 22, 1, 2, '08:45:00', '09:25:00', 1, '2017-05-12 14:48:20', '2017-05-12 14:48:20', '2017-05-12 14:48:20'),
(82, 3, 17, 1, 2, '15:01:00', '21:30:00', 1, '2017-05-10 13:48:21', '2017-05-10 13:48:21', '2017-05-10 13:48:21'),
(66, 5, 16, 1, 1, '09:00:00', '09:30:00', 2, '2017-05-06 22:33:03', '2017-05-06 22:33:03', '2017-05-06 22:33:03'),
(67, 14, 11, 4, 5, '09:30:00', '12:00:00', 2, '2017-05-07 12:04:05', '2017-05-07 12:04:05', '2017-05-07 12:04:05'),
(69, 2, 12, 2, 7, '09:30:00', '12:00:00', 2, '2017-05-07 12:05:38', '2017-05-07 12:05:38', '2017-05-07 12:05:38'),
(70, 24, 18, 5, 4, '09:30:00', '12:00:00', 2, '2017-05-07 12:06:55', '2017-05-07 12:06:55', '2017-05-07 12:06:55'),
(71, 24, 18, 4, 1, '09:30:00', '12:00:00', 2, '2017-05-07 12:14:08', '2017-05-07 12:14:08', '2017-05-07 12:14:08'),
(73, 3, 8, 2, 5, '13:30:00', '15:00:00', 2, '2017-05-07 12:15:38', '2017-05-07 12:15:38', '2017-05-07 12:15:38'),
(74, 3, 17, 5, 6, '13:30:00', '15:00:00', 2, '2017-05-07 12:15:55', '2017-05-07 12:15:55', '2017-05-07 12:15:55'),
(75, 3, 8, 2, 7, '13:30:00', '15:00:00', 2, '2017-05-07 12:17:27', '2017-05-07 12:17:27', '2017-05-07 12:17:27'),
(76, 2, 13, 4, 6, '11:00:00', '13:00:00', 2, '2017-05-07 16:26:39', '2017-05-07 16:26:39', '2017-05-07 16:26:39'),
(77, 4, 19, 3, 1, '12:00:00', '15:00:00', 2, '2017-05-07 18:14:08', '2017-05-07 18:14:08', '2017-05-07 18:14:08'),
(78, 4, 19, 1, 2, '06:30:00', '08:30:00', 1, '2017-05-07 23:35:31', '2017-05-07 23:35:31', '2017-05-07 23:35:31'),
(79, 4, 20, 3, 5, '07:30:00', '09:30:00', 1, '2017-05-08 02:30:20', '2017-05-08 02:30:20', '2017-05-08 02:30:20'),
(80, 4, 19, 3, 5, '11:30:00', '13:00:00', 1, '2017-05-08 02:31:27', '2017-05-08 02:31:27', '2017-05-08 02:31:27'),
(83, 4, 20, 1, 2, '10:01:00', '10:30:00', 1, '2017-05-10 13:48:42', '2017-05-10 13:48:42', '2017-05-10 13:48:42');

-- --------------------------------------------------------

--
-- Table structure for table `assign_course`
--

CREATE TABLE `assign_course` (
  `id` int(11) NOT NULL,
  `dept_name` varchar(50) NOT NULL,
  `teacher_name` varchar(255) NOT NULL,
  `totlal_credit` int(11) NOT NULL,
  `remaining_credit` int(11) NOT NULL,
  `course_code` varchar(10) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `course_credit` int(11) NOT NULL,
  `stat` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assign_course`
--

INSERT INTO `assign_course` (`id`, `dept_name`, `teacher_name`, `totlal_credit`, `remaining_credit`, `course_code`, `course_name`, `course_credit`, `stat`, `created_at`, `updated_at`, `deleted_at`) VALUES
(36, '4', 'Tasnif Abdullah', 40, 36, 'BBA-101', 'Fundamentals of Accounting', 4, 2, '2017-05-07 22:06:17', '2017-05-07 22:06:17', '2017-05-07 22:06:17'),
(37, '23', 'Neamat Ullah', 26, 23, 'Banagla-1', 'Bangla Bekoron ', 3, 2, '2017-05-07 22:09:28', '2017-05-07 22:09:28', '2017-05-07 22:09:28'),
(38, '4', 'Tasnif Abdullah', 40, 32, 'BBA-109', 'Analysis of Accounting Information', 4, 2, '2017-05-07 22:10:13', '2017-05-07 22:10:13', '2017-05-07 22:10:13'),
(40, '14', 'Mohiuddin Sadek', 47, -12, 'ETE-132', 'Fundamental of programming language', 4, 1, '2017-05-12 14:47:39', '2017-05-12 14:47:39', '2017-05-12 14:47:39');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `course_code` varchar(255) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `course_credit` int(11) NOT NULL,
  `body` text NOT NULL,
  `dept_id` int(11) NOT NULL,
  `semi_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course_code`, `course_name`, `course_credit`, `body`, `dept_id`, `semi_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'CSE-101', 'Fundamental Of Computer', 3, 'This is Description.', 1, 1, 1, '2017-04-26 21:27:28', '2017-04-26 21:27:28', '2017-04-26 21:27:28'),
(8, 'EE-203', 'Circuit Theory II', 3, 'N/A', 3, 3, 1, '2017-04-27 20:03:33', '2017-04-27 20:03:33', '2017-04-27 20:03:33'),
(9, 'CSE-5A', 'Introduction to Programming', 4, 'This is decription', 1, 1, 1, '2017-04-27 20:04:52', '2017-04-27 20:04:52', '2017-04-27 20:04:52'),
(10, 'CSE-8A', 'Introduction to Computer Science: Java I ', 5, 'This is description', 1, 5, 1, '2017-04-27 20:05:46', '2017-04-27 20:05:46', '2017-04-27 20:05:46'),
(12, 'IS-12', 'à¦¤à¦¾à¦«à¦¸à§€à¦°-à§§', 4, 'N/A', 2, 3, 1, '2017-04-28 23:37:03', '2017-04-28 23:37:03', '2017-04-28 23:37:03'),
(13, 'IS-203', 'à¦¹à¦¾à¦¦à§€à¦¸', 4, 'N/A', 2, 4, 1, '2017-04-29 00:23:55', '2017-04-29 00:23:55', '2017-04-29 00:23:55'),
(14, 'IS-101', 'à¦†à¦²-à¦•à§à¦°à§Ÿà¦¾à¦¨', 5, 'N/A', 2, 4, 1, '2017-04-29 02:17:23', '2017-04-29 02:17:23', '2017-04-29 02:17:23'),
(15, 'Banagla-1', 'Bangla Bekoron ', 3, 'N/a', 23, 3, 1, '2017-04-29 23:26:10', '2017-04-29 23:26:10', '2017-04-29 23:26:10'),
(16, 'CSE-201', 'Compiler Design', 3, 'N/A', 5, 4, 1, '2017-04-30 14:14:07', '2017-04-30 14:14:07', '2017-04-30 14:14:07'),
(17, 'EEE-12', 'Electrical Circuit 12', 4, 'N/A', 3, 1, 1, '2017-04-30 14:35:36', '2017-04-30 14:35:36', '2017-04-30 14:35:36'),
(19, 'BBA-101', 'Fundamentals of Accounting ', 4, 'N\\A', 4, 1, 1, '2017-05-07 18:10:43', '2017-05-07 18:10:43', '2017-05-07 18:10:43'),
(20, 'BBA-109', 'Analysis of Accounting Information', 4, 'N\\A', 4, 9, 1, '2017-05-07 18:12:41', '2017-05-07 18:12:41', '2017-05-07 18:12:41'),
(21, 'BBA-201', 'Applied Business Statistics', 4, 'New', 4, 5, 1, '2017-05-11 22:43:36', '2017-05-11 22:43:36', '2017-05-11 22:43:36'),
(22, 'ETE-132', 'Fundamental of programming language', 4, 'N\\A', 14, 9, 1, '2017-05-12 14:47:12', '2017-05-12 14:47:12', '2017-05-12 14:47:12'),
(23, 'SOCY-2280', 'Sociology of Sport', 3, 'n\\A', 24, 4, 1, '2017-05-12 14:50:53', '2017-05-12 14:50:53', '2017-05-12 14:50:53');

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `title`) VALUES
(1, 'Saturday'),
(2, 'Sunday'),
(3, 'Monday'),
(4, 'Tuesday'),
(5, 'Wednesday'),
(6, 'Thursday'),
(7, 'Friday');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `code`, `title`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CSE', 'Computer Science And Engineering', 1, '2017-04-26 02:03:03', '2017-04-26 02:03:03', '2017-04-26 02:03:03'),
(2, 'IS', 'Islamic Studies', 1, '2017-04-26 02:10:57', '2017-04-26 02:10:57', '2017-04-26 02:10:57'),
(3, 'EEE', 'Electrical And Electronics Engineering', 1, '2017-04-26 03:44:23', '2017-04-26 03:44:23', '2017-04-26 03:44:23'),
(4, 'BBB', 'Bachelor of Business Administration', 1, '2017-04-26 03:50:48', '2017-04-26 03:50:48', '2017-04-26 03:50:48'),
(5, 'CIS', 'Computer Information System', 1, '2017-04-26 03:53:37', '2017-04-26 03:53:37', '2017-04-26 03:53:37'),
(14, 'EETE', 'Electrical Electronic Telecommunication Engineering', 1, '2017-04-26 14:41:59', '2017-04-26 14:41:59', '2017-04-26 14:41:59'),
(23, 'Bangla', 'Bangla', 1, '2017-04-29 23:24:54', '2017-04-29 23:24:54', '2017-04-29 23:24:54'),
(24, 'SOCY', 'Sociology', 1, '2017-05-04 22:34:27', '2017-05-04 22:34:27', '2017-05-04 22:34:27');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `did` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`did`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Lecturer', '2017-04-27 12:04:56', '2017-04-27 12:04:56', '2017-04-27 12:04:56'),
(2, 'Assistant Professor', '2017-04-27 12:04:56', '2017-04-27 12:04:56', '2017-04-27 12:04:56'),
(3, 'Professor', '2017-04-27 12:04:56', '2017-04-27 12:04:56', '2017-04-27 12:04:56');

-- --------------------------------------------------------

--
-- Table structure for table `entrol_course`
--

CREATE TABLE `entrol_course` (
  `id` int(11) NOT NULL,
  `reg_no` varchar(50) NOT NULL,
  `course_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entrol_course`
--

INSERT INTO `entrol_course` (`id`, `reg_no`, `course_id`, `date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '6', 16, '2017-05-02', '2017-05-02 22:52:44', '2017-05-02 22:52:44', '2017-05-02 22:52:44'),
(5, '5', 22, '2017-05-02', '2017-05-02 22:52:44', '2017-05-02 22:52:44', '2017-05-02 22:52:44'),
(6, '3', 3, '2017-05-02', '2017-05-02 22:58:13', '2017-05-02 22:58:13', '2017-05-02 22:58:13'),
(7, '1', 3, '2017-05-02', '2017-05-02 23:06:34', '2017-05-02 23:06:34', '2017-05-02 23:06:34'),
(8, '1', 9, '2017-05-02', '2017-05-02 23:13:32', '2017-05-02 23:13:32', '2017-05-02 23:13:32'),
(9, '4', 15, '2017-05-03', '2017-05-03 14:23:20', '2017-05-03 14:23:20', '2017-05-03 14:23:20'),
(10, '7', 23, '2017-05-04', '2017-05-04 23:14:51', '2017-05-04 23:14:51', '2017-05-04 23:14:51'),
(11, '2', 12, '2017-05-10', '2017-05-11 00:00:39', '2017-05-11 00:00:39', '2017-05-11 00:00:39'),
(12, '2', 13, '2017-05-10', '2017-05-11 00:00:54', '2017-05-11 00:00:54', '2017-05-11 00:00:54'),
(13, '2', 14, '2017-05-10', '2017-05-11 00:01:01', '2017-05-11 00:01:01', '2017-05-11 00:01:01');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(11) NOT NULL,
  `grade` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `grade`) VALUES
(1, 'A+'),
(2, 'A'),
(3, 'A-'),
(4, 'B+'),
(5, 'B'),
(6, 'B-'),
(7, 'C+'),
(8, 'C'),
(9, 'C-'),
(10, 'D+'),
(11, 'D'),
(12, 'D-'),
(13, 'F');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `grade` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`id`, `reg_id`, `course_id`, `grade`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 1, 3, '1', '2017-05-03 01:26:32', '2017-05-03 01:26:32', '2017-05-03 01:26:32'),
(4, 1, 9, '5', '2017-05-03 01:18:48', '2017-05-03 01:18:48', '2017-05-03 01:18:48'),
(6, 5, 11, '2', '2017-05-03 13:55:25', '2017-05-03 13:55:25', '2017-05-03 13:55:25'),
(7, 4, 15, '1', '2017-05-03 14:26:45', '2017-05-03 14:26:45', '2017-05-03 14:26:45'),
(11, 2, 14, '2', '2017-05-11 20:51:38', '2017-05-11 20:51:38', '2017-05-11 20:51:38'),
(9, 6, 16, '1', '2017-05-03 16:21:12', '2017-05-03 16:21:12', '2017-05-03 16:21:12'),
(10, 7, 18, '1', '2017-05-04 23:15:17', '2017-05-04 23:15:17', '2017-05-04 23:15:17'),
(12, 2, 13, '1', '2017-05-11 21:01:47', '2017-05-11 21:01:47', '2017-05-11 21:01:47');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `room_no` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room_no`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '101', '2017-05-05 12:46:38', '2017-05-05 12:46:38', '2017-05-05 12:46:38'),
(2, '201', '2017-05-05 12:46:38', '2017-05-05 12:46:38', '2017-05-05 12:46:38'),
(3, '303', '2017-05-05 12:46:38', '2017-05-05 12:46:38', '2017-05-05 12:46:38'),
(4, '405', '2017-05-05 12:46:38', '2017-05-05 12:46:38', '2017-05-05 12:46:38'),
(5, '209', '2017-05-05 12:46:38', '2017-05-05 12:46:38', '2017-05-05 12:46:38');

-- --------------------------------------------------------

--
-- Table structure for table `semesters`
--

CREATE TABLE `semesters` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `semesters`
--

INSERT INTO `semesters` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, ' 1st Semester', '2017-04-26 16:09:48', '2017-04-26 16:09:48', '2017-04-26 16:09:48'),
(2, '2nd Semester', '2017-04-26 16:09:48', '2017-04-26 16:09:48', '2017-04-26 16:09:48'),
(3, '3rd Semester', '2017-04-26 16:10:15', '2017-04-26 16:10:15', '2017-04-26 16:10:15'),
(4, '4th Semester', '2017-04-26 16:10:15', '2017-04-26 16:10:15', '2017-04-26 16:10:15'),
(5, '5th Semester', '2017-04-26 16:11:02', '2017-04-26 16:11:02', '2017-04-26 16:11:02'),
(7, '6th Semester', '2017-04-26 16:11:02', '2017-04-26 16:11:02', '2017-04-26 16:11:02'),
(8, '7th Semester', '2017-04-26 16:11:02', '2017-04-26 16:11:02', '2017-04-26 16:11:02'),
(9, '8th Semester', '2017-04-26 16:11:02', '2017-04-26 16:11:02', '2017-04-26 16:11:02');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `email` varchar(30) NOT NULL,
  `contact` varchar(60) NOT NULL,
  `date` date NOT NULL,
  `address` varchar(255) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `reg_no` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `title`, `email`, `contact`, `date`, `address`, `dept_id`, `reg_no`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Mohiuddin Sadek', 'mohiuddin@gamil.com', '01911543307', '2017-05-01', 'Narayanganj', 1, 'CSE-2017-001', 1, '2017-05-01 20:04:31', '2017-05-01 20:04:31', '2017-05-01 20:04:31'),
(2, 'tasnif Abdullah', 'tasnif@gmail.com', '01571774573', '2017-05-01', 'Narayanganj', 2, 'IS-2017-001', 1, '2017-05-02 00:03:11', '2017-05-02 00:03:11', '2017-05-02 00:03:11'),
(3, 'Mh Suhag', 'suhag@gmail.com', '01818446677', '2017-05-01', 'Puran Dhaka', 1, 'CSE-2017-002', 1, '2017-05-02 00:04:15', '2017-05-02 00:04:15', '2017-05-02 00:04:15'),
(4, 'Abu Hanifa Rony', 'rony@gmail.com', '01928775564', '2017-05-01', 'Dhaka', 23, 'Bangla-2017-001', 1, '2017-05-02 00:55:09', '2017-05-02 00:55:09', '2017-05-02 00:55:09'),
(5, 'Rayhan Islam', 'rayhan@gmail.com', '01828990088', '2017-05-01', 'Dhaka', 14, 'EETE-2017-001', 1, '2017-05-02 01:01:03', '2017-05-02 01:01:03', '2017-05-02 01:01:03'),
(6, 'Tashahud Siddique', 'saad@gmail.com', '01670883579', '2017-05-01', 'dhaka', 5, 'CIS-2017-001', 1, '2017-05-02 01:02:38', '2017-05-02 01:02:38', '2017-05-02 01:02:38'),
(7, 'Imaran Hossine', 'imran@gmail.com', '01811543307', '2017-05-04', 'Narayanganj', 24, 'SOCY-2017-001', 1, '2017-05-04 23:13:43', '2017-05-04 23:13:43', '2017-05-04 23:13:43'),
(8, 'Robin Hood', 'robin@gmail.com', '8966858', '2017-05-07', 'Puran Dhaka', 4, 'BBB-2017-001', 1, '2017-05-07 23:45:22', '2017-05-07 23:45:22', '2017-05-07 23:45:22'),
(9, 'Abul Hossaine', 'abul@gmail.com', '879696', '2017-05-07', 'Chitogong', 4, 'BBB-2017-002', 1, '2017-05-07 23:51:02', '2017-05-07 23:51:02', '2017-05-07 23:51:02'),
(11, 'kamal', 'jamal@gmail.com', '8329827', '2017-05-11', 'hsadgh', 4, 'BBB-2017-003', 1, '2017-05-11 12:57:43', '2017-05-11 12:57:43', '2017-05-11 12:57:43');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact` varchar(60) NOT NULL,
  `desig_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `total_credit` int(11) NOT NULL,
  `remaining_credit` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `name`, `address`, `email`, `contact`, `desig_id`, `dept_id`, `total_credit`, `remaining_credit`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Sadek', 'Narayanganj', 'sadek@gmail.com', '01911543307', 1, 2, 26, 22, 0, '2017-04-27 13:08:47', '2017-04-27 13:08:47', '2017-04-27 13:08:47'),
(2, 'Mohiuddin Sadek', 'Narayanganj', 'mohiuddin@gmail.com', '01911543307', 1, 14, 47, -12, 0, '2017-04-27 13:09:39', '2017-04-27 13:09:39', '2017-04-27 13:09:39'),
(6, 'Tasnif Abdullah', 'Mizmizi', 'tasnif@gmail.com', '01571774573', 2, 4, 40, 32, 1, '2017-04-28 16:18:43', '2017-04-28 16:18:43', '2017-04-28 16:18:43'),
(7, 'Abu Hanifa', 'Bogura', 'hanifa@gmail.com', '01715289173', 2, 3, 38, 23, 1, '2017-04-28 16:53:45', '2017-04-28 16:53:45', '2017-04-28 16:53:45'),
(8, 'Tariful Islam', 'Dhaka', 'tarif@gmail.com', '01828049009', 3, 1, 29, 26, 1, '2017-04-28 19:50:20', '2017-04-28 19:50:20', '2017-04-28 19:50:20'),
(9, 'MH Suhag', 'Puran Dhaka', 'suhag@gmail.com', '01917884455', 2, 5, 26, 23, 0, '2017-04-28 19:52:04', '2017-04-28 19:52:04', '2017-04-28 19:52:04'),
(10, 'Ayesha Siddika', 'Tongi', 'ayesha@gmail.com', '01818554433', 3, 14, 150, 148, 1, '2017-04-28 19:55:14', '2017-04-28 19:55:14', '2017-04-28 19:55:14'),
(11, 'Rahmatullah', 'Dhaka', 'rahmat@gmail.com', '01566778899', 2, 14, 78, 78, 1, '2017-04-28 19:57:23', '2017-04-28 19:57:23', '2017-04-28 19:57:23'),
(12, 'Billal Hossaine', 'Dhaka', 'billal@gmail.com', '01911567789', 1, 14, 78, 78, 1, '2017-04-28 20:06:03', '2017-04-28 20:06:03', '2017-04-28 20:06:03'),
(13, 'Rayhan Vai', 'Dhaka', 'rayhan@gmail.com', '01718922433', 3, 23, 100, 97, 1, '2017-04-29 23:28:07', '2017-04-29 23:28:07', '2017-04-29 23:28:07'),
(14, 'Abu Saleh', 'Dhaka', 'saleh@gmail.com', '01811456791', 2, 1, 50, -50, 1, '2017-04-30 13:54:18', '2017-04-30 13:54:18', '2017-04-30 13:54:18'),
(15, 'Abu Noman', 'Dhaka', 'noman@gmail.com', '01911543308', 2, 4, 50, 50, 1, '2017-05-04 16:36:39', '2017-05-04 16:36:39', '2017-05-04 16:36:39'),
(16, 'Abdullah AL Amin', 'Painadi', 'abdullah@gmail.com', '01911543317', 3, 24, 100, 91, 1, '2017-05-04 22:39:53', '2017-05-04 22:39:53', '2017-05-04 22:39:53'),
(25, 'Neamat Ullah', 'painadi', 'neamat@gmail.com', '01670998854', 2, 23, 26, 23, 1, '2017-05-06 21:54:33', '2017-05-06 21:54:33', '2017-05-06 21:54:33'),
(26, 'Hijbullah Mamun', 'Power House', 'Hijbullah@gmail.com', '01911553308', 3, 4, 3, 3, 1, '2017-05-11 22:46:27', '2017-05-11 22:46:27', '2017-05-11 22:46:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `password` varchar(60) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `is_admin`, `password`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'admin@gmail.com', 1, 'admin', '2017-05-07 19:36:46', '2017-05-07 19:36:46', '2017-05-07 19:36:46'),
(2, 'student', 'student@gmail.com', 0, 'student', '2017-05-07 21:13:27', '2017-05-07 21:13:27', '2017-05-07 21:13:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allocate_rooms`
--
ALTER TABLE `allocate_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assign_course`
--
ALTER TABLE `assign_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`did`);

--
-- Indexes for table `entrol_course`
--
ALTER TABLE `entrol_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `semesters`
--
ALTER TABLE `semesters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allocate_rooms`
--
ALTER TABLE `allocate_rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `assign_course`
--
ALTER TABLE `assign_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `did` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `entrol_course`
--
ALTER TABLE `entrol_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `semesters`
--
ALTER TABLE `semesters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
